# Logboek Dataverwerkingen - UI

## Developing

Install the dependencies once with `pnpm install`. Then, start a development server:

```sh
pnpm dev
```

where the addresses of the Inzicht API backend(s) can be set in the `static/config.json` file (see example below).

## Example config file

A config file is used instead of environment variable, since the frontend is fully static and environment variables can only be set during build time. The config file can be overwritten during runtime.

Example config:

```json
{
  "backendAddresses": [
    "http://loket-inzicht-backend-127.0.0.1.nip.io:8080/v0",
    "http://shared-inzicht-backend-127.0.0.1.nip.io:8080/v0"
  ]
}
```

## Building

To create a production version of your app:

```sh
pnpm build
```

You can preview the production build with `pnpm preview`.

## Regenerating the API clients

```sh
make generate
```
