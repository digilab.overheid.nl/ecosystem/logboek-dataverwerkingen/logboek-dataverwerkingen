import Sqids from 'sqids';
import { writable } from 'svelte/store';

// Initialize Sqids
const sqids = new Sqids();

// sqidToBSN decodes a sqid to BSN. Note: does not enhance security, but is used to avoid debates about BSNs visible in browser URLs
export function sqidToBSN(id: string): string {
  return sqids.decode(id)[0].toString().padStart(9, '0'); // Note: sqids.decode(...) should always return an array with at least one element
}

// bsnToSqid is the inverse of sqidToBSN
export function bsnToSqid(bsn: string): string {
  return sqids.encode([parseInt(bsn, 10)]);
}

// Organizations store, to keep track of the organizations that are shown in the list, in order to color them consistently (and maybe for filtering on them later)
export const organizations = writable<string[]>([]);

// Store to keep track of the currently expanded log details (span ID) for activity details
export const expandedSpanId = writable<string | null>(null);

import type { Log as LDVLog } from '$lib/ldv-inzicht-api-client/types.gen';

export type Log = {
  log: LDVLog
  children: Log[]
};
