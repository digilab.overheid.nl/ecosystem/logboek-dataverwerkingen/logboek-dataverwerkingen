export const ssr = false;
export const prerender = false;

import { error } from '@sveltejs/kit';

/** @type {import('./$types').LayoutLoad} */
export async function load({ fetch }) {
  const response = await fetch('/config.json');
  if (response.ok) {
    return {
      config: await response.json(),
    };
  }

  error(500, 'Failed to fetch config.json');
}
