<script lang="ts">
  import Sidebar from '$lib/Sidebar.svelte';
  import Dropdown from '$lib/Dropdown.svelte';
  import { organizations, sqidToBSN, type Log } from '$lib';

  import { client as inzichtClient, getLogs } from '$lib/ldv-inzicht-api-client';
  import {
    client as ldvFilterClient,
    readLogboekDataVerwerkingUrlsByBsn,
  } from '$lib/ldv-filters-api-client';

  import { Bar } from 'svelte-chartjs';
  import { DateTime } from 'luxon';
  import 'chartjs-adapter-luxon';
  import { page } from '$app/stores';
  import { Chart, Tooltip, BarElement, CategoryScale, LinearScale, TimeScale } from 'chart.js';
  import zoomPlugin from 'chartjs-plugin-zoom';
  Chart.register(zoomPlugin);
  import LogRow from './LogRow.svelte';
  import './tree.pcss'; // Note: imported this way to use as global styles

  /** @type {import('./$types').LayoutData} */
  export let data: {
    config: {
      bffAddress: string;
      backendAddresses: string[];
    };
  };

  let logs: Log[] = [];
  let rootLogs: Log[] = [];
  let activityOptions: { value: string; label: string }[] = [];

  // Filters
  let dateFilter = '7d';

  // Generate bar chart data from the data returned by the Inzicht API
  let barChartData: any;
  let minDate: Date;
  let maxDate: Date;

  let debounceTimeout: NodeJS.Timeout | undefined;

  // Function to get the number of instances per day. IMPROVE: use d3.bins? But see also https://observablehq.com/@d3/d3-bin-time-thresholds
  function countLogsByHour(data: { log: { startedAt: string } }[]): {
    x: string;
    y: number;
  }[] {
    const counts = new Map<string, number>();

    data.forEach((item) => {
      // Round the time to the nearest hour
      const date = DateTime.fromISO(item.log.startedAt).toLocal().startOf('hour').toISO()!; // Note: since there may be different time zones, we convert to local time first

      const count = counts.get(date) || 0;
      counts.set(date, count + 1);
    });

    return Array.from(counts).map(([date, count]) => ({ x: date, y: count }));
  }

  Chart.register(Tooltip, BarElement, CategoryScale, LinearScale, TimeScale);

  // Update the results on filter change
  $: updateDateFilter(dateFilter);

  function updateDateFilter(dateFilter: string) {
    let since = new Date();

    switch (dateFilter) {
      case '1h':
        since.setHours(since.getHours() - 1); // Note: setHours and setDate accept negative numbers
        break;

      case '24h':
        since.setDate(since.getDate() - 1);
        break;

      case '30d':
        since.setDate(since.getDate() - 30);
        break;

      case 'custom':
        // Do nothing (handled elsewhere)
        return;

      default:
        since.setDate(since.getDate() - 7);
    }

    minDate = since;
    maxDate = new Date();
    updateData(since);
  }

  let hasFetched = false;
  let fetchError: any;

  async function updateData(since: Date, until?: Date) {
    ldvFilterClient.setConfig({
      baseUrl: `${data.config.bffAddress}/ldv-filter`,
    });

    const urls: string[] = [];

    try {
      const { data: backendData, error } = await readLogboekDataVerwerkingUrlsByBsn<false>({
        client: ldvFilterClient,
        path: {
          bsn: sqidToBSN($page.params.sqid),
        },
      });

      if (backendData) {
        urls.push(...backendData.urls);
      }
    } catch (error) {
      fetchError = error;
      return;
    }

    // Fetch the data from all backend endpoints
    logs = [];

    try {
      for (const url of urls) {
        // Set the base URL for the Inzicht API client
        inzichtClient.setConfig({
          baseUrl: `${url}/v0`,
        });

        // Fetch the logs. IMPROVE: concurrently
        const { data: backendData, error } = await getLogs<false>({
          client: inzichtClient,
          path: {
            queryID: sqidToBSN($page.params.sqid),
          },
          query: {
            since: since.toISOString(),
            until: until?.toISOString(),
          },
        });

        // Add the response data to the data
        if (backendData) {
          logs.push(
            ...backendData.data.map((item): Log => {
              return { log: item, children: [] };
            }),
          );
        }

        fetchError = error;
      }

      // Sort the logs by start Time
      logs.sort(
        (a, b) => new Date(b.log.startedAt).getTime() - new Date(a.log.startedAt).getTime(),
      );

      // Store the unique organizations and first/last times
      const orgSet: Set<string> = new Set();
      logs.forEach((item) => {
        orgSet.add(item.log.organization);
      });
      organizations.set(Array.from(orgSet).sort());

      // Activity filter. Note: we filter by activity name and not by activity ID, since the user only sees the name (and two processing activities in different organizations may have the same name)
      activityOptions = [
        { value: '', label: 'Alle verwerkingsactiviteiten' },
        ...[...new Set(logs.map((item) => item.log.processingActivity))]
          .filter(Boolean) // Filter out empty strings
          .sort()
          .map((activity) => ({
            value: activity,
            label: activity,
          })),
      ];
    } catch (err) {
      fetchError = err;
      return;
    }

    // Set hasFetched to true
    hasFetched = true;
  }

  // Organisations filter
  $: organizationOptions = [
    { value: '', label: 'Alle organisaties' },
    ...$organizations.map((org, i) => ({ value: i.toString(), label: org })),
  ];

  let organisationFilter = '';

  let activityFilter = '';

  // Filtered logs. Note: filtered in the frontend. IMPROVE: filter in the backend?
  $: filteredLogs = logs.filter(
    (item) =>
      (!organisationFilter ||
        item.log.organization === $organizations[parseInt(organisationFilter, 10)]) &&
      (!activityFilter || item.log.processingActivity === activityFilter),
  );

  $: updateRootLogs(filteredLogs);

  function updateRootLogs(filteredLogs: Log[]) {
    rootLogs = [];

    // Build tree
    filteredLogs.forEach((item) => {
      // If a log has no foreign trace ID and no parent span, it is a root log
      if (!item.log.tags['dpl.rva.foreign.trace_id'] && !item.log.parentSpanId) {
        rootLogs.push(item);
        return;
      }

      let parent: Log | undefined;

      if (item.log.parentSpanId) {
        parent = filteredLogs.find(
          (log: Log) =>
            log.log.traceId === item.log.traceId && log.log.spanId === item.log.parentSpanId,
        );
      } else if (item.log.tags['dpl.rva.foreign.trace_id']) {
        parent = filteredLogs.find(
          (log: Log) => log.log.traceId === item.log.tags['dpl.rva.foreign.trace_id'],
        );
      }

      if (parent) {
        parent.children.push(item);
      } else {
        rootLogs.push(item);
      }
    });

    rootLogs = resplice(rootLogs);
  }

  function resplice(logs: Log[]): Log[] {
    let n: Log[] = [];
    logs.forEach((log) => {
      log.children = resplice(log.children);

      if (log.log.processingActivityId !== '00000000-0000-0000-0000-000000000000') {
        n.push(log);
        return;
      }

      log.children.forEach((child, index) => {
        if (child.log.traceId === log.log.traceId) {
          log.children.splice(index, 1);
          child.children.push(...log.children);
          n.push(child);
        }
      });
    });

    return n;
  }

  // Pagination
  let pageNumber = 1;
  const rowsPerPage = 10;
  const onEachSide = 1;
  const onEnds = 2;
  $: numPages = Math.max(Math.ceil(rootLogs.length / rowsPerPage), 1);

  $: pages = ((pageNumber: number, numPages: number) => {
    // In case of a short range, return it
    if (numPages <= (onEachSide + onEnds) * 2) {
      return rng(1, numPages);
    }

    let res: number[] = [];

    if (pageNumber > onEachSide + onEnds + 2) {
      res = [
        ...rng(1, onEnds),
        0, // Ellipsis
        ...rng(pageNumber - onEachSide, pageNumber),
      ];
    } else {
      res = rng(1, pageNumber);
    }

    if (pageNumber < numPages - onEachSide - onEnds - 1) {
      return [
        ...res,
        ...rng(pageNumber + 1, pageNumber + onEachSide),
        0, // Ellipsis
        ...rng(numPages - onEnds + 1, numPages),
      ];
    }

    return [...res, ...rng(pageNumber + 1, numPages)];
  })(pageNumber, numPages);

  // Helper function to create ranges
  function rng(from: number, to: number): number[] {
    let result: number[] = [];
    for (let i = from; i <= to; i++) {
      result.push(i);
    }
    return result;
  }

  // Bar chart data
  $: barChartData = {
    datasets: [
      {
        label: 'Aantal dataverwerkingen',
        barThickness: 6,
        data: countLogsByHour(filteredLogs), // IMPROVE: depending on the scale, count by hour/minute, etc.
        backgroundColor: [
          'rgba(59, 130, 246, 0.3)', // Tailwind blue-500/20
        ],
        borderColor: [
          '#3b82f6', // Tailwind blue-500
        ],
        borderWidth: 1,
      },
    ],
  };

  $: visibleRootLogs = rootLogs.slice((pageNumber - 1) * rowsPerPage, pageNumber * rowsPerPage);
</script>

<svelte:head>
  <title>Mijn Fictieve Overheid</title>
</svelte:head>

<div class="container mx-auto grid grid-cols-12 gap-16 pb-5 pt-2.5">
  <div class="col-span-3"><Sidebar active="dataverwerkingen" /></div>

  <div class="col-span-9 text-sm">
    <p class="mb-2 flex items-center text-sm uppercase">
      <span class="text-sky-800">Home</span>
      <svg
        class="mx-2 h-2.5 w-2.5"
        viewBox="0 0 32 32"
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        ><path
          transform="rotate(180 16.444 16)"
          fill="currentColor"
          d="m22.853 27.401-.064.065a1.823 1.823 0 0 1-2.579 0L10.034 17.29a1.823 1.823 0 0 1 0-2.58L20.21 4.534a1.823 1.823 0 0 1 2.579 0l.064.065a1.823 1.823 0 0 1 0 2.579L14.031 16l8.822 8.822a1.823 1.823 0 0 1 0 2.58"
        /></svg
      >
      <span class="text-sky-800">Dataverwerkingen</span>
    </p>

    <h1 class="mb-5 text-3xl font-[350]">Dataverwerkingen</h1>

    <div class="rounded-sm bg-white p-8 shadow-[0_0_6px_rgba(0,0,0,0.16)]">
      <div class="mb-5">
        <Dropdown
          options={[
            { value: 'custom', label: 'Aangepast', isHidden: true },
            { value: '1h', label: 'Laatste uur' },
            { value: '24h', label: 'Laatste 24 uur' },
            { value: '7d', label: 'Laatste 7 dagen' },
            { value: '30d', label: 'Laatste 30 dagen' },
          ]}
          extraClasses="mr-2 mb-2"
          bind:value={dateFilter}
        />

        <Dropdown
          options={organizationOptions}
          bind:value={organisationFilter}
          extraClasses="mr-2 mb-2"
        />

        <Dropdown options={activityOptions} bind:value={activityFilter} extraClasses="mb-2" />
      </div>

      {#if logs}
        <div class="mb-5 h-20">
          <Bar
            data={barChartData}
            options={{
              responsive: true,
              maintainAspectRatio: false,
              animation: {
                duration: 0,
              },
              scales: {
                x: {
                  type: 'time',
                  time: {
                    unit: 'day',
                    displayFormats: {
                      minute: 'HH:mm', // Format for minute ticks
                    },
                  },
                  ticks: {
                    autoSkip: false,
                    maxRotation: 0,
                  },
                  min: minDate.toISOString(),
                  max: maxDate.toISOString(),
                },
                y: {
                  ticks: {
                    display: false,
                  },
                  min: 0,
                },
              },
              plugins: {
                zoom: {
                  zoom: {
                    wheel: {
                      enabled: true,
                      speed: 0.2,
                    },
                    drag: {
                      enabled: true,
                    },
                    pinch: {
                      enabled: true,
                    },
                    mode: 'x',
                    onZoom({ chart }) {
                      const xScale = chart.scales['x'];
                      minDate = new Date(xScale.min);
                      maxDate = new Date(xScale.max);

                      // Update the date filter label
                      dateFilter = 'custom';

                      // Debounce updating the data to avoid multiple requests in a short time when scroll zooming. Note: onZoomComplete also fires multiple events
                      clearTimeout(debounceTimeout);
                      debounceTimeout = setTimeout(() => {
                        updateData(minDate, maxDate);
                      }, 500);
                    },
                  },
                },
              },
            }}
          />
        </div>

        <div class="flex items-center font-semibold">
          <div class="flex-grow">Organisatie</div>
          <div class="w-2/3">Verwerkingsactiviteit</div>
        </div>

        <ul id="tree">
          {#each visibleRootLogs as log (log.log.traceId)}
            <LogRow {log} />
          {:else}
            <li class="before:hidden after:hidden !-ml-3 !pl-0 !border-l-0">
              {#if fetchError}
                Fout bij ophalen van gegevens: {fetchError}
              {:else if hasFetched}
                Geen dataverwerkingen voor deze periode.
              {:else}
                Laden…
              {/if}
            </li>
          {/each}
        </ul>

        <nav class="mt-6">
          <ul
            class="inline-flex -space-x-px rounded-md text-sm font-medium shadow-sm"
            aria-label="Paginanummering"
          >
            <li>
              <button
                on:click={() => {
                  if (pageNumber > 1) {
                    pageNumber -= 1;
                  }
                }}
                class="ml-0 flex h-full items-center justify-center rounded-l-md border border-slate-300 bg-white p-2 {pageNumber >
                1
                  ? 'hover:bg-slate-100 hover:text-slate-700'
                  : 'cursor-default text-gray-400'}"
              >
                <svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                  ><path
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="m15 6l-6 6l6 6"
                  /></svg
                >
                <span class="sr-only">Vorige</span>
              </button>
            </li>
            {#each pages as page}
              <li>
                {#if page}
                  <button
                    on:click={() => {
                      pageNumber = page;
                    }}
                    class="flex h-full items-center justify-center border px-4 py-2 {page ===
                    pageNumber
                      ? 'relative border-blue-600 bg-blue-500 text-white'
                      : 'border-slate-300 bg-white hover:bg-slate-100 hover:text-slate-700'}"
                    >{page}</button
                  >
                {:else}
                  <span
                    class="flex h-full items-center justify-center border border-slate-300 bg-white px-4 py-2 text-gray-400"
                    >…</span
                  >
                {/if}
              </li>
            {/each}
            <li>
              <button
                on:click={() => {
                  if (pageNumber < numPages) {
                    pageNumber += 1;
                  }
                }}
                class="ml-0 flex h-full items-center justify-center rounded-r-md border border-slate-300 bg-white p-2 {pageNumber <
                numPages
                  ? 'hover:bg-slate-100 hover:text-slate-700'
                  : 'cursor-default text-gray-400'}"
              >
                <span class="sr-only">Volgende</span>
                <svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                  ><path
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="m9 6l6 6l-6 6"
                  /></svg
                >
              </button>
            </li>
          </ul>
        </nav>
      {/if}
    </div>
  </div>
</div>
