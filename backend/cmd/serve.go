package cmd

import (
	"fmt"

	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/config"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/pkg/clickhouse"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/rva"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/rva/api"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/rva/static"
)

type ServeCmd struct {
	BackendListenAddress string `env:"APP_BACKEND_LISTEN_ADDRESS" default:":8080" name:"backend-listen-address" help:"Address to listen  on."`
	Organization         string `env:"APP_ORGANIZATION" name:"organization" help:"The name of the organization."`
	Clickhouse           struct {
		Endpoint string `env:"APP_QUERY_ENDPOINT" default:"clickhouse:9000" name:"endpoint" help:"Address of the log query endpoint."`
		Database string `env:"APP_QUERY_DATABASE" default:"otel" name:"database" help:"Database of the log query"`
		User     string `env:"APP_QUERY_USER" default:"digilab" name:"user" help:"User of the log query"`
		Password string `env:"APP_QUERY_PASSWORD" name:"password" help:"Password of the log query"`
	} `embed:"" prefix:"clickhouse-"`
	RVA struct {
		Current string `env:"APP_RVA_CURRENT" name:"current" enum:"static,api" required:""`
		Static  struct {
			FilePath string `env:"APP_RVA_STATIC_FILEPATH" name:"file-path" help:"The file path of the static config set"`
		} `embed:"" prefix:"static-"`
		API struct {
			Endpoint string `env:"APP_RVA_API_ENDPOINT" name:"endpoint" help:"The endpoint of the RVA API"`
		} `embed:"" prefix:"api-"`
	} `embed:"" prefix:"rva-"`
}

func (opt *ServeCmd) Run(ctx *Context) error {
	logger := ctx.Logger.With("application", "http_server")

	logger.Info("Starting fictief inzicht backend", "config", opt)

	cfg := config.Config{
		RVA: config.RVA{
			Current: opt.RVA.Current,
			Static: &config.RVAStatic{
				FilePath: opt.RVA.Static.FilePath,
			},
			API: &config.RVAAPI{
				Endpoint: opt.RVA.API.Endpoint,
			},
		},
		Clickhouse: config.Clickhouse{
			Endpoint: opt.Clickhouse.Endpoint,
			Database: opt.Clickhouse.Database,
			User:     opt.Clickhouse.User,
			Password: opt.Clickhouse.Password,
		},
	}

	ch, err := clickhouse.New(logger, &cfg.Clickhouse)
	if err != nil {
		logger.Error("failed to create query backend", "err", err)
		return err
	}

	rva, err := NewRVA(cfg.RVA)
	if err != nil {
		return err
	}

	app, err := application.New(logger, ch, rva, opt.BackendListenAddress, opt.Organization)
	if err != nil {
		logger.Error("failed to create applicatoin", "err", err)
		return err
	}

	app.Router()

	if err := app.ListenAndServe(); err != nil {
		logger.Error("listen and serve failed", "err", err)
		return err
	}

	return nil
}

func NewRVA(cfg config.RVA) (rva.RVA, error) {
	var a rva.RVA
	switch cfg.Current {
	case "static":
		static, err := static.New(cfg.Static)
		if err != nil {
			return nil, fmt.Errorf("create static: %w", err)
		}

		a = static
	case "api":
		ab, err := api.New(cfg.API)
		if err != nil {
			return nil, fmt.Errorf("create api: %w", err)
		}

		a = ab
	}

	return a, nil
}
