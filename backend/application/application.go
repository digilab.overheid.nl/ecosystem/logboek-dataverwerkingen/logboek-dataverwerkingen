package application

import (
	"log/slog"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"

	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/interface/api"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/pkg"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/rva"
)

type Application struct {
	*http.Server
	logger       *slog.Logger
	querier      pkg.Querier
	organization string
	rva          rva.RVA
}

func New(logger *slog.Logger, queryBackend pkg.Querier, rva rva.RVA, backendListenAddress, organization string) (*Application, error) {
	return &Application{
		Server: &http.Server{
			Addr: backendListenAddress,
		},
		querier:      queryBackend,
		logger:       logger,
		organization: organization,
		rva:          rva,
	}, nil
}

func (app *Application) Router() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	// Enable CORS
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"}, // IMPROVE: limit origins using this attribute or AllowOriginFunc
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Indicates how long (in seconds) the results of a preflight request can be cached
	}))

	r.Use(func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			app.logger.Debug("Request headers", "headers", r.Header)
			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	})
	r.Use(middleware.Recoverer)
	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(middleware.SetHeader("Content-Type", "application/json"))

	apiHandler := api.NewStrictHandler(app, nil)
	r.Mount("/v0", api.Handler(apiHandler))

	r.Group(func(r chi.Router) {
		r.Use(middleware.SetHeader("Content-Type", "application/yaml"))
		r.Get("/openapi.yaml", app.OpenAPI)
	})

	app.Handler = r
}
