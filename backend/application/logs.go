package application

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/interface/api"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/pkg"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/rva"
)

// GetLogs implements api.StrictServerInterface.
func (app *Application) GetLogs(ctx context.Context, request api.GetLogsRequestObject) (api.GetLogsResponseObject, error) {
	app.logger.Info("Received request to get logs", "query_id", request.QueryID)

	since := time.Now().AddDate(0, 0, -7)
	if request.Params.Since != nil {
		since = *request.Params.Since
	}

	until := time.Now()
	if request.Params.Until != nil {
		until = *request.Params.Until
	}

	records, err := app.querier.Query(ctx, request.QueryID, pkg.Query{Since: since, Until: until})
	if err != nil {
		app.logger.Error("failed to query backend", "err", err)
		return api.GetLogs500JSONResponse{
			InternalErrorResponseJSONResponse: api.InternalErrorResponseJSONResponse{
				Errors: []api.Error{
					{Message: err.Error()},
				},
			},
		}, err
	}

	app.logger.Debug("amount of logs", "amount", len(records))

	logs := make([]api.Log, 0, len(records))
	for _, log := range records {
		activity, err := app.rva.GetProcessingActivity(ctx, log.ProcessingActivityID.String())
		if err != nil {
			if !errors.Is(err, rva.ErrNotFound) {
				return nil, fmt.Errorf("get processing activity: %w", err)
			}

			app.logger.Warn("could not find activity", "id", log.ProcessingActivityID.String())
		}

		app.logger.Debug("activity", "activity", activity)

		var personalData []string
		if activity.PersonalDataCategories != nil {
			personalData = *activity.PersonalDataCategories
		}

		var activityName string
		if activity.Name != nil {
			activityName = *activity.Name
		}

		log := api.Log{
			Organization:         app.organization,
			ParentSpanId:         log.ParentSpanID,
			PersonalData:         personalData,
			ProcessingActivity:   activityName,
			ProcessingActivityId: log.ProcessingActivityID,
			SpanId:               log.SpanID,
			StartedAt:            log.StartedAt,
			Duration:             log.Duration,
			Tags:                 log.Tags,
			TraceId:              log.TraceID,
			StatusCode:           api.LogStatusCode(log.StatusCode),
			StatusMessage:        log.StatusMessage,
		}

		logs = append(logs, log)
	}

	return api.GetLogs200JSONResponse{
		LogResponseJSONResponse: api.LogResponseJSONResponse{
			Data: logs,
		},
	}, nil
}
