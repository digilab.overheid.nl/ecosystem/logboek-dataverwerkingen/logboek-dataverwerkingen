FROM --platform=$BUILDPLATFORM digilabpublic.azurecr.io/golang:1.23.3-alpine3.19 as builder

ARG TARGETOS TARGETARCH

# Cache dependencies
RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]
RUN ["go", "install", "github.com/deepmap/oapi-codegen/v2/cmd/oapi-codegen@v2.2.0"]

WORKDIR /build
COPY go.mod go.sum ./

# Cache go modules
RUN go mod download

WORKDIR /build/interface
COPY interface ./

# Generate server based on openapi spec
RUN oapi-codegen -config ./config.yaml https://gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/-/raw/main/api/openapi.yaml?ref_type=heads

## Build the Go Files
WORKDIR /build
COPY . .

## Run the server for dev
ENTRYPOINT CompileDaemon -log-prefix=false -build="go build -o server ." -command="./server serve"
