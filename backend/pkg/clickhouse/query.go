package clickhouse

import (
	"context"
	"fmt"
	"log/slog"

	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/config"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/pkg"

	"github.com/ClickHouse/ch-go"
	"github.com/ClickHouse/ch-go/proto"
	"github.com/google/uuid"
)

var _ pkg.Querier = &ClickhouseBackend{}

type ClickhouseBackend struct {
	logger *slog.Logger
	cfg    *config.Clickhouse
}

func New(logger *slog.Logger, cfg *config.Clickhouse) (*ClickhouseBackend, error) {
	logger.Info("connected to database")

	return &ClickhouseBackend{
		logger: logger,
		cfg:    cfg,
	}, nil
}

var query = `
WITH TracesWithUser AS (
    SELECT DISTINCT TraceId
    FROM otel_traces
    WHERE SpanAttributes['dpl.core.user'] = {userId: String} AND
	Timestamp >= {since: DateTime64(6, 'UTC')} AND 
	Timestamp <= {until: DateTime64(6, 'UTC')}
)
SELECT 
    Timestamp,
    Duration,
    TraceId,
    SpanId,
    ParentSpanId,
    SpanName,
    SpanAttributes,
    SpanAttributes['dpl.rva.activity.id'] as rvaActivityId,
    StatusCode,
    StatusMessage
FROM otel_traces
WHERE TraceId IN (SELECT TraceId FROM TracesWithUser) AND 
	(SpanAttributes['dpl.core.user'] = {userId: String} OR SpanAttributes['dpl.core.user'] = '')
ORDER BY Timestamp;
`

// Query implements pkg.QueryBackend.
func (c *ClickhouseBackend) Query(ctx context.Context, id string, qry pkg.Query) ([]pkg.Log, error) {
	conn, err := connect(context.Background(), c.cfg.Endpoint, c.cfg.Database, c.cfg.User, c.cfg.Password)
	if err != nil {
		return nil, err
	}

	defer conn.Close()

	var protolog struct {
		time                    proto.ColDateTime64
		duration                proto.ColUInt64
		traceId                 proto.ColStr
		spanId                  proto.ColStr
		parentSpanId            proto.ColStr
		spanName                proto.ColLowCardinality[string]
		spanAttributes          proto.ColMap[string, string]
		spanAttributeActivityID proto.ColStr
		statusCode              proto.ColLowCardinality[string]
		statusMessage           proto.ColStr
	}

	protolog.spanName = *new(proto.ColStr).LowCardinality()
	protolog.spanAttributes = *proto.NewMap(new(proto.ColStr).LowCardinality(), new(proto.ColStr))
	protolog.statusCode = *new(proto.ColStr).LowCardinality()

	logs := make([]pkg.Log, 0)

	query := ch.Query{
		Body: query,
		Parameters: []proto.Parameter{
			{Key: "userId", Value: fmt.Sprintf("'%s'", id)},
			{Key: "since", Value: fmt.Sprintf("'%s'", qry.Since.UTC().Format("2006-01-02 15:04:05.000000"))},
			{Key: "until", Value: fmt.Sprintf("'%s'", qry.Until.UTC().Format("2006-01-02 15:04:05.000000"))},
		},
		Result: proto.Results{
			{Name: "Timestamp", Data: &protolog.time},
			{Name: "Duration", Data: &protolog.duration},
			{Name: "TraceId", Data: &protolog.traceId},
			{Name: "SpanId", Data: &protolog.spanId},
			{Name: "ParentSpanId", Data: &protolog.parentSpanId},
			{Name: "SpanName", Data: &protolog.spanName},
			{Name: "SpanAttributes", Data: &protolog.spanAttributes},
			{Name: "rvaActivityId", Data: &protolog.spanAttributeActivityID},
			{Name: "StatusCode", Data: &protolog.statusCode},
			{Name: "StatusMessage", Data: &protolog.statusMessage},
		},
		// OnResult will be called on next received data block.
		OnResult: func(ctx context.Context, b proto.Block) error {
			for idx := range b.Rows {

				tags := make(map[string]any, len(protolog.spanAttributes.Row(idx)))
				for k, v := range protolog.spanAttributes.Row(idx) {
					tags[k] = v
				}

				var paID uuid.UUID
				if protolog.spanAttributeActivityID.Row(idx) != "" {
					// Parse the processing activity ID, which should be a UUID
					paID, err = uuid.Parse(protolog.spanAttributeActivityID.Row(idx))
					if err != nil {
						return fmt.Errorf("error decoding processing activity ID: %w", err)
					}
				}

				logs = append(logs, pkg.Log{
					ProcessingActivityID: paID,
					StartedAt:            protolog.time.Row(idx),
					Duration:             protolog.duration.Row(idx),
					TraceID:              protolog.traceId.Row(idx),
					SpanID:               protolog.spanId.Row(idx),
					ParentSpanID:         protolog.parentSpanId.Row(idx),
					Tags:                 tags,
					StatusCode:           protolog.statusCode.Row(idx),
					StatusMessage:        protolog.statusMessage.Row(idx),
				})
			}

			return nil
		},
		OnLogs: func(ctx context.Context, l []ch.Log) error {
			for _, v := range l {
				c.logger.DebugContext(ctx, "on logs", "log", v)
			}

			return nil
		},
	}

	if err := conn.Do(ctx, query); err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}

	return logs, nil
}

func connect(ctx context.Context, url, db, user, password string) (*ch.Client, error) {
	options := ch.Options{
		Address:  url,
		Database: db,
		User:     user,
		Password: password,
	}

	client, err := ch.Dial(ctx, options)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	if err := client.Ping(ctx); err != nil {
		return nil, err
	}

	return client, nil
}
