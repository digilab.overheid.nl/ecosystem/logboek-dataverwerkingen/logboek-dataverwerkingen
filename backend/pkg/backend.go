package pkg

import (
	"context"
	"time"

	"github.com/google/uuid"
)

type Log struct {
	ProcessingActivityID uuid.UUID
	StartedAt            time.Time
	Duration             uint64
	TraceID              string
	SpanID               string
	ParentSpanID         string
	StatusCode           string
	StatusMessage        string
	Tags                 map[string]interface{}
}

type Query struct {
	Since time.Time
	Until time.Time
}

type Querier interface {
	Query(ctx context.Context, id string, query Query) ([]Log, error)
}
