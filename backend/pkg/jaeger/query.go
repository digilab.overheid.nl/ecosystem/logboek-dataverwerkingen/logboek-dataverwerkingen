package jaeger

import (
	"context"
	"encoding/json"
	"log/slog"
	"net/http"
	"net/url"

	pkg "gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/pkg"
)

var _ pkg.Querier = &JaegerBackend{}

// API_PATH for Zipkin API
var API_PATH = "/api/traces"

type JaegerBackend struct {
	logger *slog.Logger
	client *http.Client
	url    string
}

func New(logger *slog.Logger, url string) (*JaegerBackend, error) {
	return &JaegerBackend{
		url:    url,
		client: http.DefaultClient,
		logger: logger,
	}, nil
}

// Query implements pkg.QueryBackend.
func (b JaegerBackend) Query(ctx context.Context, id string, _ pkg.Query) ([]pkg.Log, error) {
	tags := map[string]string{
		"dpl.core.user": id,
	}
	jsonTags, err := json.Marshal(tags)
	if err != nil {
		return nil, err
	}

	queryUrl := url.URL{}
	queryUrl.Scheme = "http"
	queryUrl.Host = b.url
	queryUrl.Path = API_PATH
	query := queryUrl.Query()

	query.Set("tags", string(jsonTags))
	queryUrl.RawQuery = query.Encode()

	resp, err := b.client.Get(queryUrl.String())
	if err != nil {
		return nil, err
	}

	response := JaegerResponse{}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return nil, err
	}

	// _, err = io.ReadAll(resp.Body)
	// if err != nil {
	// 	return nil, err
	// }

	// b.logger.Info("Querying Jaeger", "url", queryUrl.String(), "status", resp.Status)

	results := []pkg.Log{}
	for _, trace := range response.Data {
		for _, span := range trace.Spans {

			parentSpanID := ""
			if len(span.References) > 0 {
				for _, ref := range span.References {
					if ref.RefType == "CHILD_OF" {
						parentSpanID = ref.SpanID
					}
				}
			}

			tags := map[string]interface{}{}
			for _, tag := range span.Tags {
				tags[tag.Key] = tag.Value
			}

			// b.logger.Info("Querying Jaeger", "span", span, "tags", tags)
			results = append(results, pkg.Log{
				TraceID:      trace.TraceID,
				SpanID:       span.SpanID,
				ParentSpanID: parentSpanID,
				Tags:         tags,
			})
		}
	}

	// b.logger.Info("Querying Jaeger", "results", len(results))

	return results, err
}
