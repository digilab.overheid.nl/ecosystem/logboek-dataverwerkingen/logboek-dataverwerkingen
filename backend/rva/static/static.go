package static

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/config"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/model"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/rva"
	"gopkg.in/yaml.v3"
)

var _ rva.RVA = &Static{}

type Static struct {
	cfg                  *config.RVAStatic
	processingActivities map[string]model.RVA
}

func New(cfg *config.RVAStatic) (*Static, error) {
	activities, err := parseFile(cfg.FilePath)
	if err != nil {
		return nil, fmt.Errorf("parse file: %w", err)
	}

	return &Static{
		cfg:                  cfg,
		processingActivities: activities,
	}, nil
}

// GetProcessingActivity implements rva.RVA.
func (s *Static) GetProcessingActivity(ctx context.Context, id string) (model.RVA, error) {
	activities, ok := s.processingActivities[id]
	if !ok {
		return model.RVA{}, rva.ErrNotFound
	}

	return activities, nil
}

func parseFile(endpoint string) (map[string]model.RVA, error) {
	var t struct {
		Activities map[string]model.RVA
	}

	data, err := os.ReadFile(endpoint)
	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(data, &t); err != nil {
		return nil, err
	}

	return t.Activities, nil
}
