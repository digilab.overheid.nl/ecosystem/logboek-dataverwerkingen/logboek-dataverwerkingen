package rva

import (
	"context"
	"errors"

	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/model"
)

var ErrNotFound = errors.New("not found")

type RVA interface {
	GetProcessingActivity(ctx context.Context, id string) (model.RVA, error)
}
