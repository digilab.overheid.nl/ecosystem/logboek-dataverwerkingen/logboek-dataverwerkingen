package api

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/config"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/model"
	"gitlab.com/digilab.overheid.nl/ecosystem/logboek-dataverwerkingen/logboek-dataverwerkingen/backend/rva"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/rva-api"
)

var _ rva.RVA = &API{}

type API struct {
	client *api.ClientWithResponses
}

func New(cfg *config.RVAAPI) (*API, error) {
	client, err := api.NewClientWithResponses(cfg.Endpoint)
	if err != nil {
		return nil, fmt.Errorf("new client with responses: %w", err)
	}

	return &API{
		client: client,
	}, nil
}

// GetProcessingActivity implements rva.RVA.
func (api *API) GetProcessingActivity(ctx context.Context, id string) (model.RVA, error) {
	resp, err := api.client.GetProcessingActivityWithResponse(ctx, id)
	if err != nil {
		return model.RVA{}, fmt.Errorf("get processing activity: %w", err)
	}

	if resp.StatusCode() != http.StatusOK {
		return model.RVA{}, fmt.Errorf("get processing activity: %s", resp.Body)
	}

	if resp.JSON200 == nil {
		return model.RVA{}, fmt.Errorf("get processing activity: no data")
	}

	data := *resp.JSON200

	var lb *model.LegalBasis
	if data.LegalBasis != nil {
		b := model.LegalBasis(*data.LegalBasis)
		lb = &b
	}

	return model.RVA{
		Controller:                 data.Controller,
		DataSubjectCategories:      data.DataSubjectCategories,
		EnvisagedTimeLimit:         data.EnvisagedTimeLimit,
		EnvisagedTimeLimitDuration: data.EnvisagedTimeLimitDuration,
		Id:                         data.Id,
		LegalBasis:                 lb,
		LegalBasisComment:          data.LegalBasisComment,
		Name:                       data.Name,
		PersonalDataCategories:     data.PersonalDataCategories,
		Purpose:                    data.Purpose,
		RecipientsCategories:       data.RecipientsCategories,
		Uri:                        data.Uri,
	}, nil
}
