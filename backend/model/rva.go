package model

// Defines values for LegalBasis.
const (
	CONSENT               LegalBasis = "CONSENT"
	LEGALOBLIGATION       LegalBasis = "LEGAL_OBLIGATION"
	LEGITIMATEINTERESTS   LegalBasis = "LEGITIMATE_INTERESTS"
	PERFORMANCEOFCONTRACT LegalBasis = "PERFORMANCE_OF_CONTRACT"
	PUBLICINTEREST        LegalBasis = "PUBLIC_INTEREST"
	VITALINTERESTS        LegalBasis = "VITAL_INTERESTS"
)

// LegalBasis As defined in the GDPR, article 6, section 1
type LegalBasis string

// ProcessingActivityItem defines model for ProcessingActivityItem.
type RVA struct {
	Controller                 *string   `json:"controller,omitempty"`
	DataSubjectCategories      *[]string `json:"data_subject_categories,omitempty"`
	EnvisagedTimeLimit         *string   `json:"envisaged_time_limit,omitempty"`
	EnvisagedTimeLimitDuration *string   `json:"envisaged_time_limit_duration,omitempty"`
	Id                         *string   `json:"id,omitempty"`

	// LegalBasis As defined in the GDPR, article 6, section 1
	LegalBasis             *LegalBasis `json:"legal_basis,omitempty"`
	LegalBasisComment      *string     `json:"legal_basis_comment,omitempty"`
	Name                   *string     `json:"name,omitempty"`
	PersonalDataCategories *[]string   `json:"personal_data_categories,omitempty"`
	Purpose                *string     `json:"purpose,omitempty"`
	RecipientsCategories   *[]string   `json:"recipients_categories,omitempty"`
	Uri                    *string     `json:"uri,omitempty"`
}
