package config

type Config struct {
	RVA        RVA
	Clickhouse Clickhouse
}

type RVA struct {
	Current string
	Static  *RVAStatic
	API     *RVAAPI
}

type Clickhouse struct {
	Endpoint string
	Database string
	User     string
	Password string
}

type RVAStatic struct {
	FilePath string
}

type RVAAPI struct {
	Endpoint string
}
